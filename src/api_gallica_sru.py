from lxml import etree
from tqdm import tqdm
import requests
import csv
import os
import re

from .utils.constants import IN, OUT, NS_SRU
from .utils.functions import validate_url, list_to_pipe


# -------------------------------------------------------
# read the input csv, retrieve data from the Gallica SRU 
# API using requests and write the output to a new csv.
# -------------------------------------------------------


def url_processing(url: str) -> [bool, str, str, str]:
    """
    process a gallica ARK url:
    - check that a gallica url is valid 
    - clean the url: sometimes, extra characters are added;
      at other times, the link is a link to a IIIF manifest.
      in those cases, rebuild a clean url
    - extract the ark identifier (the part of 
      the ark id that is unique to the ressource,
      after /12148/)
    - build the url pointing to the IIIF manifest
    
    along with the processed urls is returned a `valid` boolean.
    this boolean indicates if the urls are valid and if the api
    should be queried (aka, if the `gallica_url` returns a valid 
    response.). this means that the API will be queried even if a
    iiif manifest isn't valid
    
    :returns: `valid` `ark`, `gallica_url`, `manifest_url`.
    """
    valid = True  # to check an api query should be run
    ark = ""  # unique ark id
    gallica_url = ""  # url to the gallica page
    manifest_url = ""  # url to the iiif manifest 
    r = re.search("https?://gallica.bnf.fr/ark:/12148/([a-z0-9]+)", url)
    
    # first case scenario: it is a normal ark url
    if r:  
        ark = r[1]
        gallica_url = r[0]
        manifest_url = f"https://gallica.bnf.fr/iiif/ark:/12148/{ark}/manifest.json"

        # validate the urls extracted/created
        if not validate_url(gallica_url):
            gallica_url = url  # rollback to the original url
            valid = False
        if not validate_url(manifest_url):
            manifest_url = ""
    
    else:
        # second case scenario: it is a iiif manifest url
        r_manifest = re.search("https?://gallica.bnf.fr/iiif/ark:/12148/([a-z0-9]+)(/[a-z0-9]+)?/manifest.json", url)
        if r_manifest:
            ark = r_manifest[1]
            gallica_url = f"https://gallica.bnf.fr/ark:/12148/{ark}"
            manifest_url = f"https://gallica.bnf.fr/iiif/ark:/12148/{ark}/manifest.json"
            # print(gallica_url)
        
            # validate urls extracted / created
            if not validate_url(gallica_url):
                gallica_url = url  # rollback to the original url
                valid = False
            if not validate_url(manifest_url):
                manifest_url = ""
                
        # third case scenario: the url isn't valid. assign it to 
        # `gallica_url` and don't rebuilt other urls
        else:
            valid = False
            gallica_url = url

    return [valid, ark, gallica_url, manifest_url]


def api_request(ark: str) -> str:
    """
    run a request on the API and return the response
    as a string.
    
    :param ark: the ark idenfier (only the unique string part)
    """
    r = requests.get(f"https://gallica.bnf.fr/SRU?version=1.2&operation=searchRetrieve&query=dc.identifier any '{ark}'")
    if r.status_code == 200:
        out = r.content
    else:
        print(r.headers, "\n", r.content)
        out = ""
    return out
    

def process_response(r_body: str, ark: str) -> [str]:
    """
    parse the response xml, extract relevant data
    and return it as a list
    
    :param r_body: the response's body: a string representation of an xml
    :returns:
    """
    # output variables
    title_1 = ""       # dc:title       |   colonne `Titre`
    title_2 = ""       # dc:title       |   colonne `Autre titre`
    author_1 = ""      # dc:creator     |   colonne `Auteur 1`
    author_2 = ""      # dc:creator     |   colonne ` Auteur 2`
    publisher = ""     # dc:publisher   |   colonne `Éditeur`
    date = ""          # dc:date        |   colonne `Date`
    technique = ""     # dc:type        |   colonne `Technique`
    inventory_no = ""  # dc:source      |   colonne `N° d'inventaire`
    relation = ""      # dc:relation    |   colonne `Relation`
    description = ""   # dc:description |   colonne `Description iconographique` 
    # marques et inscriptions ne sont pas renvoyées dans l'API sru et pas présents dans le Dublin Core
    
    if r_body != "":
        tree = etree.fromstring(r_body)
        try:
            body = tree.xpath("//srw:searchRetrieveResponse//srw:record[1]//oai_dc:dc[1]", namespaces=NS_SRU)[0]
            try: title_1 = body.xpath("//dc:title/text()", namespaces=NS_SRU)[0]
            except IndexError: pass
            
            try: title_2 = body.xpath("//dc:title/text()", namespaces=NS_SRU)[1]
            except IndexError: pass
            
            try: author_1 = body.xpath("//dc:creator/text()", namespaces=NS_SRU)[0]
            except IndexError: pass
            
            try: author_2 = body.xpath("//dc:creator/text()", namespaces=NS_SRU)[1]
            except IndexError: pass
            
            try: publisher = list_to_pipe(
                body.xpath("//dc:publisher/text()", namespaces=NS_SRU)
            )
            except IndexError: pass
            
            try: date = list_to_pipe(
                body.xpath("//dc:date/text()", namespaces=NS_SRU)
            )
            except IndexError: pass
            
            try: technique = list_to_pipe(
                body.xpath("//dc:type/text()", namespaces=NS_SRU)
            )
            except IndexError: pass
            
            try: inventory_no = body.xpath("//dc:source/text()", namespaces=NS_SRU)[0]
            except IndexError: pass
            
            try: relation = list_to_pipe(
                body.xpath("//dc:relation/text()", namespaces=NS_SRU)
            )
            except IndexError: pass
            
            try: description = list_to_pipe(
                body.xpath("//dc:relation/text()", namespaces=NS_SRU)
            )
            except IndexError: pass
        except IndexError:
            print(f"""
            
                UNDEXPECTED RESPONSE BODY FOR ON {ark}
                
                PLEASE CHECK THAT NO RESPONSE BODY WAS EXPECTED
            
            """)
            print(r_body)
            # sys.exit(1)
        
    return title_1, title_2, author_1, author_2\
           , publisher, date, technique, inventory_no\
           , relation, description
    
    
def gallica_pipeline():
    """
    main algorithm: 
    - read the rows from the csv
    - run a request on each `ark` url
    - parse the response to extract data
    - write it to an output csv file.
    """
    fh_in = open(os.path.join(IN, "iconographie_gallica_in.csv"), mode="r")
    fh_out = open(os.path.join(OUT, "iconographie_gallica_out.csv"), mode="w")
    reader = csv.reader(fh_in, delimiter="\t", quotechar="'")
    writer = csv.writer(fh_out, delimiter="\t", quotechar="'")
    trows = sum(1 for row in reader)
    fh_in.seek(0)
    firstrow = True
    
    for row in tqdm(reader, desc="retrieving data from the API", total=trows):
        if firstrow == True:
            # don't process the first row but write it as a header (see bottom)
            firstrow = False
        else:
            # only process rows where data hasn't been manually 
            # added beyond the urls and the tags at the end
            if all(re.search("^\s*$", r) for r in row[4:15]):
                valid, ark, gallica_url, manifest_url = url_processing(row[2])
            
                # only process rows where valid arks have been retrieved
                if valid:
                    r_body = api_request(ark)
                    title_1, title_2, author_1, author_2\
                    , publisher, date, technique, inventory_no\
                    , relation, description = process_response(r_body, ark)
                        
                else:
                    title_1 = title_2 = author_1 = author_2\
                    = publisher = date = technique = inventory_no\
                    = relation = description = ""
                        
                # define the output row; even if `valid==False`, data extracted
                # in the `url_processing()` is used here
                row = [row[0], row[1], manifest_url, gallica_url, title_1
                       , title_2, author_1, author_2, publisher, date, row[10]
                       , technique, inventory_no, relation, description, row[15]
                       , row[16], row[17], row[18], row[19], row[20]]
            
            # if infos have aldready been manually added to the dataset,
            # - create a `IIIF` for `row[2]` from the Gallica Ark url
            #   (for simplicity, we do this using `url_processing()`.
            # - move `row[2]` to `row[3]`: there is a mistake in the source
            #   dataset where Gallica Arks are in the ManifestIIIF column. 
            #  this is changed by the output, and the row permutation should
            #  not be done if this script is reused once new items have been 
            #  added to this dataset
            else:
                valid, ark, gallica_url, manifest_url = url_processing(row[2])
                row = [row[0], row[1], manifest_url, gallica_url, row[4], row[5]
                       , row[6], row[7], row[8], row[9], row[10], row[11]
                       , row[12], row[13], row[14], row[15], row[16], row[17]
                       , row[18], row[19], row[20]]
                
        # write the output row
        writer.writerow(row)
    
    fh_in.close()
    fh_out.close()
    return
