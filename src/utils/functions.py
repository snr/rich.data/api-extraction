import requests
import re

# -------------------------------
# globally useful functions
# -------------------------------

def list_to_pipe(_in: list):
    """
    transform a list into a string of pipe-separated values.
    
    ["a", "b", "c"] => "a|b|c"
    """
    out = ""
    for i in _in:
        out += f"{i}|"
    out = re.sub("\|$", "", out)
    return out


def validate_url(url: str) -> bool:
    """
    validate a url to ensure that our data is clean. run a 
    `GET` request using the url. if an http code other than 
    200-299 is returned, return false.
    
    :returns: true if a valid response is returned, false if not.
    """
    r = requests.get(url) 
    if not (200 <= r.status_code <= 299):
        out = False
    else:
        out = True
    return out