import os

# paths to files / directories
UTILS = os.path.abspath(os.path.dirname(__file__))
SRC = os.path.abspath(os.path.join(UTILS, os.pardir))
ROOT = os.path.abspath(os.path.join(SRC, os.pardir))
IN = os.path.abspath(os.path.join(ROOT, "in"))
OUT = os.path.abspath(os.path.join(ROOT, "out"))

NS_SRU = {
    # default for Catalogues SRU: "http://catalogue.bnf.fr/namespaces/InterXMarc"
    "ixm": "http://catalogue.bnf.fr/namespaces/InterXMarc",
    "ns6": "http://gallica.bnf.fr/namespaces/gallica/",
    "mn": "http://catalogue.bnf.fr/namespaces/motsnotices",
    "sd": "http://www.loc.gov/zing/srw/diagnostic/",
    "diag": "http://www.loc.gov/zing/srw/diagnostic/",
    "mxc": "info:lc/xmlns/marcxchange-v2",
    "dc": "http://purl.org/dc/elements/1.1/",
    "oai_dc": "http://www.openarchives.org/OAI/2.0/oai_dc/",
    "onix": "http://www.editeur.org/onix/2.1/reference/",
    "srw": "http://www.loc.gov/zing/srw/",
    "onix_dc": "http://bibnum.bnf.fr/NS/onix_dc/",
}  # xml namespaces for an SRU api response