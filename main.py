import argparse
import sys
import os

from src.utils.constants import OUT
from src.api_gallica_sru import gallica_pipeline

# -----------------------------------------------------
# command line interface
# -----------------------------------------------------


if __name__ == "__main__":
    _help = """
        usage: python main.py [-g]
        
        about: pipeline to extract data from an API using a unique 
        identifier in a csv dataset
        
        options:
        * -h : show a help message and exit
        * -g : retrieve data from the Gallica SRU api
    """
    # create necessary directories
    if not os.path.isdir(OUT):
        os.makedirs(OUT)
        
    # define arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-g",
                        help="retrieve data from the Gallica SRU api",
                        action="store_true")
    
    # run
    if len(sys.argv) == 1:
        sys.exit(_help)
    args = parser.parse_args()
    if args.g:
        gallica_pipeline()
