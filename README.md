# RICH.DATA - API EXTRACTION

cette étape vise à **l'enrichissement automatique** de jeux de données à l'aide
de ressources accessibles via des API. en utilisant l'identifiant unique d'un
item (un identifiant ARK, par exemple), un jeu de données est reconstruit sur
mesure.

l'enrichissement automatique constitue **la première étape** de la chaîne de
traitement `rich.data`, avant la normalisation et la préparation des jeux de 
données complets (voir **`data preparation`**), la transformation des jeux de
données en une base de données `PostGreSQL` (voir **`data 2 sql`**) et avant
l'utilisation de ces jeux de données par l'application du projet Richelieu
(voir **`application prototype`**).

---

## Installation et utilisation

```bash
git clone https://gitlab.inha.fr/snr/rich.data/api-extraction.git  # cloner le dépôt
cd api-extraction/  # se déplacer dans le dépôt
python3 -m venv env  # créer un environnement virtuel
source env/bin/activate  # sourcer l'environnement
pip install -r requirements.txt  # installer les dépendances
python main.py -g  # requêter l'API Gallica BnF
```

les **jeux de données utilisés en entrée** de ces scripts sont placés 
dans le dossier [**`in/`**](./in). celui ci comprend:
- un fichier `.xlsx` qui est le tableur comportant les métadonnées 
  sur toutes les ressources iconographiques.
- des fichiers `.csv` qui sont exportés de ce tableur et qui contiennent
  les métadonnées sur les ressources d'une seule institution. ce sont
  ces tableurs qui sont utilisés en entrée des scripts de moissonnage
  d'APIs.

les **jeux de données produits** par cette étape de la chaîne de traitement
se trouvent dans le dossier [**`out/`**](./out). ce dossier comprend:
- un ou plusieurs fichiers `.csv` qui correspondent au résultat produit
  par cette étape.
- un fichier `.xlsx` qui correspond au tableur comprenant toutes les 
  métadonnées iconographiques, avec éventuellement des corrections
  manuelles des données présentes dans le `.csv`.

---

## Fonctionnement interne

à l'aide d'un identifiant unique (URI `ARK`, par exemple), une API est requêtée pour
constituer un jeu de données sur mesure. pour l'instant, cet enrichissement automatique
n'a été fait que avec les données issues de `Gallica/BnF`, mais il pourrait être étendu
à d'autres, si les institutions conservant les ressources mettent des API à disposition
(c'est le cas de Paris Musées).

---

## Crédits et license

scripts développés par Paul Kervegan, identifiants ARK récupérés par Charlotte Duvette,
Justine Gain, Esther Davila et Loïc Jeanson à l'INHA.


